﻿using CorticaIntegration.CorticaData.Cameras;
using CorticaIntegration.CorticaData.Event;
using CorticaIntegration.CorticaData.PoiDb;
using CorticaIntegration.CorticaData.PoiDb.Parameters;
using CorticaIntegration.CorticaData.PoiDb.Shared;
using CorticaIntegration.EventArguments;
using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Serializers.SystemTextJson;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.WebSockets;
using System.Reflection;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace CorticaIntegration
{
    public class Cortica
    {
        public string ServerName { get; set; }
        public string ServerAbbrv { get; set; }
        public string CameraApiUrl { get; set; }
        public string PoiApiUrl { get; set; }
        public string ListenerUrl { get; private set; }
        public bool PoiEnrollSelected { get; set; }
        public bool FaceSearchSelected { get; set; }
        public bool Online { get; private set; } = false;
        public DateTime LastComms { get; private set; } = DateTime.MinValue;

        #region Events

        public int BufferSize { get; set; } = 1000;

        private HttpListener listener;
        private bool runServer = true;

        public event EventHandler<CorticaEventArgs> CorticaEvent;
        public static event EventHandler<CorticaErrorEventArgs> Error;

        public void ConnectListener()
        {
            ConnectListener(ListenerUrl);
        }

        public void ConnectListener(string listenerUrl)
        {
            ListenerUrl = listenerUrl;

            if (listener != null)
                listener.Close();

            listener = new HttpListener();
            listener.Prefixes.Add(listenerUrl);
            listener.Start();

            runServer = true;
            Task listenTask = HandleIncomingConnections();
        }

        public void DisconnectListener()
        {
            if (listener != null)
                listener.Close();

            runServer = false;
        }

        private async Task HandleIncomingConnections()
        {
            Stopwatch stopwatch = new Stopwatch();

            while (runServer)
            {
                HttpListenerContext ctx = await listener.GetContextAsync();

                // Peel out the requests and response objects
                HttpListenerRequest req = ctx.Request;

                stopwatch.Start();

                string msgString = "";

                //while (true)
                //{
                //    byte[] buffer = new byte[BufferSize];
                //    int bytesread = req.InputStream.Read(buffer, 0, BufferSize);
                //    msgString += Encoding.ASCII.GetString(buffer, 0, bytesread);

                //    if (bytesread < BufferSize)
                //    {
                //        ctx.Response.Close();

                //        break;
                //    }
                //}

                var reader = new System.IO.StreamReader(req.InputStream, req.ContentEncoding);
                msgString = reader.ReadToEnd();
                reader.Close();

                EventData event_data = null;

                if (!string.IsNullOrWhiteSpace(msgString))
                {
                    var msgString2 = msgString.Replace(@"\", "").Trim('"');

                    try
                    {
                        event_data = JsonSerializer.Deserialize<EventData>(msgString2, new JsonSerializerOptions { IgnoreNullValues = true, PropertyNameCaseInsensitive = true,  });
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine($"Error deserializing event data: {msgString2} \n {msgString} - {ex.Message}");
                        Error?.Invoke(this, new CorticaErrorEventArgs() { Message = $"Error deserializing event data: {msgString2} \n {msgString}", Error = ex });
                    }
                }

                stopwatch.Stop();
                Debug.WriteLine($"Read buffer in: {stopwatch.ElapsedMilliseconds}ms");
                stopwatch.Reset();

                //we only care about non-status events
                if (event_data != null && !event_data.Event_type.Equals("status", StringComparison.InvariantCultureIgnoreCase))
                {
                    CorticaEvent?.Invoke(this, new CorticaEventArgs() { Event_data = event_data });
                }
            }
        } 

        public async Task ReadEvent(Stream stream)
        {
            string msgString = "";

            using (var reader = new StreamReader(stream))
            {
                var body = await reader.ReadToEndAsync();

                msgString = body;
            }

            EventData event_data = null;

            if (!string.IsNullOrWhiteSpace(msgString))
            {
                msgString = msgString.Replace(@"\", "").Trim('"');

                try
                {
                    event_data = JsonSerializer.Deserialize<EventData>(msgString, new JsonSerializerOptions { IgnoreNullValues = true, PropertyNameCaseInsensitive = true });
                }
                catch (Exception ex)
                {
                    Debug.WriteLine($"Error Serializing event data: {msgString} - {ex.Message}");
                    Error?.Invoke(this, new CorticaErrorEventArgs() { Message = $"Error Serializing event data: {msgString}", Error = ex });
                }
            }

            //we only care about non-status events
            if (event_data != null && !event_data.Event_type.Equals("status", StringComparison.InvariantCultureIgnoreCase))
            {
                CorticaEvent?.Invoke(this, new CorticaEventArgs() { Event_data = event_data });
            }
        }
        #endregion

        #region POI
        /// <summary>
        /// Get list of all poi_ids 
        /// </summary>
        /// <param name="ServerAddress">[http]|[https]://[IP address]:[Port]</param>
        /// <returns></returns>
        public static async Task<PoisGet> GetPois(string ServerAddress, dynamic AccessToken)
        {
            var client = new RestClient(ServerAddress);
            client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
            client.UseSystemTextJson(new JsonSerializerOptions() { IgnoreNullValues = true, PropertyNameCaseInsensitive = true });
            var request = new RestRequest("poi_db/poi/", Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("authorization", "Bearer " + AccessToken);

            var response = await client.ExecuteGetAsync<PoisGet>(request);

            Debug.WriteLine(response.Content);

            if (response.ErrorException != null)
                Error?.Invoke(null, new CorticaErrorEventArgs() { Error = response.ErrorException, Message = $"{MethodBase.GetCurrentMethod().ReflectedType.Name}: {response.ErrorMessage}" });

            return response.Data;
        }

        /// <summary>
        /// Get list of POI properties, associated watchlist_ids, face_ids, and face properties, including crop images (if GetCrops = true)
        /// </summary>
        /// <param name="ServerAddress">[http]|[https]://[IP address]:[Port]</param>
        /// <param name="PoiIds">Unique ids of POIs to get</param>
        /// <param name="GetCrops">Flag indicating whether to return face crop images. ​Default​: false</param>
        /// <returns></returns>
        public static async Task<PoiGet> GetPoi(string ServerAddress, dynamic AccessToken, IEnumerable<string> PoiIds, bool GetCrops = true)
        {
            var client = new RestClient(ServerAddress);
            client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
            client.UseSystemTextJson(new JsonSerializerOptions() { IgnoreNullValues = true, PropertyNameCaseInsensitive = true });
            var request = new RestRequest("poi_db/poi/get/", Method.GET);

            //Authorization with Access Token.
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("authorization", "Bearer " + AccessToken);
            request.AddJsonBody(new { pois = from p in PoiIds select new { poi_id = p }, get_crops = GetCrops });

            var response = await client.ExecutePostAsync<PoiGet>(request);

            Debug.WriteLine(response.Content);

            if (response.ErrorException != null)
                Error?.Invoke(null, new CorticaErrorEventArgs() { Error = response.ErrorException, Message = $"{MethodBase.GetCurrentMethod().Name}: {response.ErrorMessage}" });

            return response.Data;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ServerAddress">[http]|[https]://[IP address]:[Port]</param>
        /// <param name="Pois">List of poi properties</param>
        /// <param name="WatchlistIds">Unique ids of watchlists with which to associate new POIs</param>
        /// <returns></returns>
        public static async Task<PoiCreate> CreatePoi(string ServerAddress, dynamic AccessToken, IEnumerable<PoiCreateParameter> Pois, IEnumerable<string> WatchlistIds = null)
        {
            var client = new RestClient(ServerAddress);
            client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
            client.UseSystemTextJson(new JsonSerializerOptions() { IgnoreNullValues = true, PropertyNameCaseInsensitive = true });
            var request = new RestRequest("poi_db/poi/", Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("authorization", "Bearer " + AccessToken);
            request.AddJsonBody(new { pois = from p in Pois select new { display_name = p.Display_name, display_img = p.Display_img.Split(',').Last(), save_face = p.Save_face, use_as_face = p.Use_as_face }, watchlist_ids = WatchlistIds });

            var response = await client.ExecutePostAsync<PoiCreate>(request);

            Debug.WriteLine(response.Content);

            if (response.ErrorException != null)
                Error?.Invoke(null, new CorticaErrorEventArgs() { Error = response.ErrorException, Message = $"{MethodBase.GetCurrentMethod().ReflectedType.Name}: {response.ErrorMessage}" });

            return response.Data;
        }

        /// <summary>
        /// Update the POI properties (not including watchlist/crop associations)
        /// </summary>
        /// <param name="ServerAddress">[http]|[https]://[IP address]:[Port]</param>
        /// <param name="PoiId">Unique id of the POI</param>
        /// <param name="PoiName">Display name of POI that will be available in match events and bbox stream</param>
        /// <param name="PoiImage">String base64 image face crop to be displayed on match events</param>
        /// <returns></returns>
        public static async Task<PoiUpdate> UpdatePoi(string ServerAddress, dynamic AccessToken, string PoiId, string PoiName, string PoiImage)
        {
            var client = new RestClient(ServerAddress);
            client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
            client.UseSystemTextJson(new JsonSerializerOptions() { IgnoreNullValues = true, PropertyNameCaseInsensitive = true });
            var request = new RestRequest("poi_db/poi/{poi_id}", Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("authorization", "Bearer " + AccessToken);
            request.AddUrlSegment("poi_id", PoiId);
            request.AddJsonBody(new { display_name = PoiName, display_img = PoiImage });

            var response = await client.ExecuteAsync<PoiUpdate>(request);

            Debug.WriteLine(response.Content);

            if (response.ErrorException != null)
                Error?.Invoke(null, new CorticaErrorEventArgs() { Error = response.ErrorException, Message = $"{MethodBase.GetCurrentMethod().ReflectedType.Name}: {response.ErrorMessage}" });

            return response.Data;
        }

        /// <summary>
        /// Remove POIs, and remove all crops associated with those POIs.
        /// POIs will no longer be associated with any watchlist.
        /// </summary>
        /// <param name="ServerAddress">[http]|[https]://[IP address]:[Port]</param>
        /// <param name="Pois">List of POIs to remove</param>
        /// <returns></returns>
        public static async Task<PoiRemove> RemovePoi(string ServerAddress, dynamic AccessToken, IEnumerable<string> Pois)
        {
            var client = new RestClient(ServerAddress);
            client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
            client.UseSystemTextJson(new JsonSerializerOptions() { IgnoreNullValues = true, PropertyNameCaseInsensitive = true });
            var request = new RestRequest("poi_db/poi/remove/", Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("authorization", "Bearer " + AccessToken);
            request.AddJsonBody(new { pois = from p in Pois select new { poi_id = p } });

            var response = await client.ExecutePostAsync<PoiRemove>(request);

            Debug.WriteLine(response.Content);

            if (response.ErrorException != null)
                Error?.Invoke(null, new CorticaErrorEventArgs() { Error = response.ErrorException, Message = $"{MethodBase.GetCurrentMethod().ReflectedType.Name}: {response.ErrorMessage}" });

            return response.Data;
        }

        /// <summary>
        /// Add POIs to watchlist.
        /// </summary>
        /// <param name="ServerAddress">[http]|[https]://[IP address]:[Port]</param>
        /// <param name="WatchlistID">Unique id of the watchlist from which to associate POIs</param>
        /// <param name="PoiIds">Unique ids of the POIs to add. If POI is already associated with watchlist there will ​not​ be an error (it'll stay associated to watchlist)</param>
        public static async Task<PoiAddToWatchlist> AddToWatchlist(string ServerAddress, dynamic AccessToken, string WatchlistID, IEnumerable<string> PoiIds)
        {
            var client = new RestClient(ServerAddress);
            client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
            client.UseSystemTextJson(new JsonSerializerOptions() { IgnoreNullValues = true, PropertyNameCaseInsensitive = true });
            var request = new RestRequest("poi_db/poi/watchlist_add/", Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("authorization", "Bearer " + AccessToken);
            request.AddJsonBody(new { watchlist_id = WatchlistID, pois = from p in PoiIds select new { poi_id = p } });

            var response = await client.ExecutePostAsync<PoiAddToWatchlist>(request);

            Debug.WriteLine(response.Content);

            return response.Data;
        }

        /// <summary>
        /// Removes the association of these POIs to a watchlist.
        /// </summary>
        /// <param name="ServerAddress">[http]|[https]://[IP address]:[Port]</param>
        /// <param name="WatchlistID">Unique id of the watchlist from which to disassociate this POI</param>
        /// <param name="PoiIds">Unique ids of the POIs to remove from watchlist</param>
        /// <returns></returns>
        public static async Task<PoiRemoveFromWatchlist> RemoveFromWatchlist(string ServerAddress, dynamic AccessToken, string WatchlistID, IEnumerable<string> PoiIds)
        {
            var client = new RestClient(ServerAddress);
            client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
            client.UseSystemTextJson(new JsonSerializerOptions() { IgnoreNullValues = true, PropertyNameCaseInsensitive = true });
            var request = new RestRequest("poi_db/poi/watchlist_remove/", Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("authorization", "Bearer " + AccessToken);
            request.AddJsonBody(new { watchlist_id = WatchlistID, pois = from p in PoiIds select new { poi_id = p } });

            var response = await client.ExecutePostAsync<PoiRemoveFromWatchlist>(request);

            Debug.WriteLine(response.Content);

            if (response.ErrorException != null)
                Error?.Invoke(null, new CorticaErrorEventArgs() { Error = response.ErrorException, Message = $"{MethodBase.GetCurrentMethod().ReflectedType.Name}: {response.ErrorMessage}" });

            return response.Data;
        }
        #endregion

        #region Watchlists
        /// <summary>
        /// Create a new watchlist.
        /// </summary>
        /// <param name="ServerAddress">[http]|[https]://[IP address]:[Port]</param>
        /// <param name="Watchlist"><para>Threshold_delta​ ​Optional​ (float) - Threshold offset added to Stream Threshold (set via configuration and Streams API) for this watchlist. ​Default​ ​value:​ 0</para>
        /// <para>Watchlist_type​ ​Optional​(string) - can be either “whitelist” or “blacklist”. Defines whether alerts will be sent on POI appearance(“blacklist”) or on appearance of a person that is not found in watchlist(“whitelist”). ​Default value​: “blacklist”</para>
        /// <para>Display_name​ ​Optional​(string) - display name of watchlist to be displayed in events and bbox stream. ​Default value​: null</para>
        /// <para>Display_color ​Optional​ (string) - display color of watchlist. Format: hex characters representing RGB values (e.g.“#132ef5”). ​Default value​: null</para></param>
        /// <returns></returns>
        public static async Task<WatchlistCreate> CreateWatchlist(string ServerAddress, dynamic AccessToken, WatchlistCreateParameter Watchlist)
        {
            var client = new RestClient(ServerAddress);
            client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
            client.UseSystemTextJson(new JsonSerializerOptions() { IgnoreNullValues = true, PropertyNameCaseInsensitive = true });
            var request = new RestRequest("poi_db/watchlist/", Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("authorization", "Bearer " + AccessToken);
            request.AddJsonBody(new { display_name = Watchlist.Display_name, watchlist_type = Watchlist.Watchlist_type, threshold_delta = Watchlist.Threshold_delta, display_color = Watchlist.Display_color });

            var response = await client.ExecutePostAsync<WatchlistCreate>(request);

            Debug.WriteLine(response.Content);

            if (response.ErrorException != null)
                Error?.Invoke(null, new CorticaErrorEventArgs() { Error = response.ErrorException, Message = $"{MethodBase.GetCurrentMethod().ReflectedType.Name}: {response.ErrorMessage}" });

            return response.Data;
        }

        /// <summary>
        /// Update the watchlist parameters.
        /// </summary>
        /// <param name="ServerAddress">[http]|[https]://[IP address]:[Port]</param>
        /// <param name="WatchlistID">Unique id of the Watchlist to update</param>
        /// <param name="Watchlist"><para>Threshold_delta​ ​Optional​ (float) - Threshold offset added to Stream Threshold (set via configuration and Streams API) for this watchlist. ​Default​ ​value:​ 0</para>
        /// <para>Watchlist_type​ ​Optional​(string) - can be either “whitelist” or “blacklist”. Defines whether alerts will be sent on POI appearance(“blacklist”) or on appearance of a person that is not found in watchlist(“whitelist”). ​Default value​: “blacklist”</para>
        /// <para>Display_name​ ​Optional​(string) - display name of watchlist to be displayed in events and bbox stream. ​Default value​: null</para>
        /// <para>Display_color ​Optional​ (string) - display color of watchlist. Format: hex characters representing RGB values (e.g.“#132ef5”). ​Default value​: null</para></param>
        /// <returns></returns>
        public static async Task<WatchlistCreate> UpdateWatchlist(string ServerAddress, dynamic AccessToken, string WatchlistID, WatchlistCreateParameter Watchlist)
        {
            var client = new RestClient(ServerAddress);
            client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
            client.UseSystemTextJson(new JsonSerializerOptions() { IgnoreNullValues = true, PropertyNameCaseInsensitive = true });
            var request = new RestRequest("poi_db/watchlist/{watchlist_id}", Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("authorization", "Bearer " + AccessToken);
            request.AddUrlSegment("watchlist_id", WatchlistID);
            request.AddJsonBody(new { display_name = Watchlist.Display_name, watchlist_type = Watchlist.Watchlist_type, threshold_delta = Watchlist.Threshold_delta, display_color = Watchlist.Display_color });

            var response = await client.ExecutePostAsync<WatchlistCreate>(request);

            Debug.WriteLine(response.Content);

            if (response.ErrorException != null)
                Error?.Invoke(null, new CorticaErrorEventArgs() { Error = response.ErrorException, Message = $"{MethodBase.GetCurrentMethod().ReflectedType.Name}: {response.ErrorMessage}" });

            return response.Data;
        }

        /// <summary>
        /// Remove an empty watchlist. Request will be rejected if watchlist is not empty of POIs
        /// </summary>
        /// <param name="ServerAddress">[http]|[https]://[IP address]:[Port]</param>
        /// <param name="WatchlistID">The unique id of the watchlist to be removed</param>
        /// <returns></returns>
        public static async Task<WatchlistRemove> RemoveWatchlist(string ServerAddress, dynamic AccessToken, string WatchlistID)
        {
            var client = new RestClient(ServerAddress);
            client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
            client.UseSystemTextJson(new JsonSerializerOptions() { IgnoreNullValues = true, PropertyNameCaseInsensitive = true });
            var request = new RestRequest("poi_db/watchlist/{watchlist_id}", Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("authorization", "Bearer " + AccessToken);
            request.AddUrlSegment("watchlist_id", WatchlistID);
            
            var response = await client.ExecuteAsync<WatchlistRemove>(request);

            Debug.WriteLine(response.Content);

            if (response.ErrorException != null)
                Error?.Invoke(null, new CorticaErrorEventArgs() { Error = response.ErrorException, Message = $"{MethodBase.GetCurrentMethod().ReflectedType.Name}: {response.ErrorMessage}" });

            return response.Data;
        }

        /// <summary>
        /// Get watchlist properties and list of associated pois
        /// </summary>
        /// <param name="ServerAddress">[http]|[https]://[IP address]:[Port]</param>
        /// <param name="WatchlistID">Watchlist Id</param>
        /// <returns></returns>
        public static async Task<WatchlistGet> GetWatchlist(string ServerAddress, dynamic AccessToken, string WatchlistID)
        {
            var client = new RestClient(ServerAddress);
            client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
            client.UseSystemTextJson(new JsonSerializerOptions() { IgnoreNullValues = true, PropertyNameCaseInsensitive = true });
            var request = new RestRequest("poi_db/watchlist/{watchlist_id}", Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("authorization", "Bearer " + AccessToken);
            request.AddUrlSegment("watchlist_id", WatchlistID);

            var response = await client.ExecuteGetAsync<WatchlistGet>(request);

            Debug.WriteLine(response.Content);

            if (response.ErrorException != null)
                Error?.Invoke(null, new CorticaErrorEventArgs() { Error = response.ErrorException, Message = $"{MethodBase.GetCurrentMethod().ReflectedType.Name}: {response.ErrorMessage}" });

            return response.Data;
        }

        /// <summary>
        /// Retrieves list of all watchlist ids in the system.
        /// </summary>
        /// <param name="ServerAddress">[http]|[https]://[IP address]:[Port]</param>
        /// <returns></returns>
        public async Task<WatchlistsGet> GetWatchlists(string ServerAddress, dynamic AccessToken)
        {
            try
            {
                var client = new RestClient(ServerAddress);
                client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                client.UseSystemTextJson(new JsonSerializerOptions() { IgnoreNullValues = true, PropertyNameCaseInsensitive = true });
                var request = new RestRequest("poi_db/watchlist/", Method.GET);
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("authorization", "Bearer " + AccessToken);

                var response = await client.ExecuteGetAsync<WatchlistsGet>(request);

                Debug.WriteLine(response.Content);

                if (response.ErrorException != null)
                    Error?.Invoke(null, new CorticaErrorEventArgs() { Error = response.ErrorException, Message = $"{MethodBase.GetCurrentMethod().ReflectedType.Name}: {response.ErrorMessage}" });

                Online = true;
                LastComms = DateTime.Now;

                return response.Data;
            }
            catch (Exception ex)
            {
                Online = false;

                Error?.Invoke(null, new CorticaErrorEventArgs() { Message = $"Error in GetWatchlists for {ServerAddress}: {ex.Message}", Error = ex });
                return null;
            }
        }
        #endregion

        #region Faces
        /// <summary>
        /// Add faces to the DB. Faces ​must​ be associated with a POI. 
        /// POI DB saves a irreversible signature representing the face(e.g.face image cannot be re-obtained from signature). 
        /// Saving face crop image is optional.
        /// </summary>
        /// <param name="ServerAddress">[http]|[https]://[IP address]:[Port]</param>
        /// <param name="Faces">List of faces to add</param>
        /// <returns></returns>
        public static async Task<FaceAdd> AddFace(string ServerAddress, dynamic AccessToken, IEnumerable<FaceAddParameter> Faces)
        {
            var client = new RestClient(ServerAddress);
            client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
            client.UseSystemTextJson(new JsonSerializerOptions() { IgnoreNullValues = true, PropertyNameCaseInsensitive = true });
            var request = new RestRequest("poi_db/face/", Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("authorization", "Bearer " + AccessToken);
            request.AddJsonBody(new { faces = from f in Faces select new { img = f.Img, poi_id = f.Poi_id, save_crop = f.Save_crop } });

            var response = await client.ExecutePostAsync<FaceAdd>(request);

            Debug.WriteLine(response.Content);

            if (response.ErrorException != null)
            {
                Error?.Invoke(null, new CorticaErrorEventArgs() { Error = response.ErrorException, Message = $"{MethodBase.GetCurrentMethod().ReflectedType.Name}: {response.ErrorMessage}" });
            }
            return response.Data;
        }

        /// <summary>
        /// Remove faces from the DB. If as a result of this operation, a POI record is no longer associated with any face, the POI still remains in the system.
        /// </summary>
        /// <param name="ServerAddress">[http]|[https]://[IP address]:[Port]</param>
        /// <param name="FacesIds">Faces to remove</param>
        /// <returns></returns>
        public static async Task<FaceRemove> RemoveFace(string ServerAddress, dynamic AccessToken, IEnumerable<string> FacesIds)
        {
            var client = new RestClient(ServerAddress);
            client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
            client.UseSystemTextJson(new JsonSerializerOptions() { IgnoreNullValues = true, PropertyNameCaseInsensitive = true });
            var request = new RestRequest("poi_db/face/remove/", Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("authorization", "Bearer " + AccessToken);
            request.AddJsonBody(new { faces = from f in FacesIds select new { face_id = f } });

            var response = await client.ExecutePostAsync<FaceRemove>(request);

            Debug.WriteLine(response.Content);

            if (response.ErrorException != null)
            {
                Error?.Invoke(null, new CorticaErrorEventArgs() { Error = response.ErrorException, Message = $"{MethodBase.GetCurrentMethod().ReflectedType.Name}: {response.ErrorMessage}" });
            }
            return response.Data;
        }

        /// <summary>
        /// Get info for faces, including face crop image, if stored.
        /// </summary>
        /// <param name="ServerAddress">[http]|[https]://[IP address]:[Port]</param>
        /// <param name="FacesIds">Faces to retrieve</param>
        /// <returns></returns>
        public static async Task<FaceGet> GetFace(string ServerAddress, dynamic AccessToken, IEnumerable<string> FacesIds)
        {
            var client = new RestClient(ServerAddress);
            client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
            client.UseSystemTextJson(new JsonSerializerOptions() { IgnoreNullValues = true, PropertyNameCaseInsensitive = true });
            var request = new RestRequest("poi_db/face/get/", Method.GET);
            request.AddHeader("authorization", "Bearer " + AccessToken);
            request.AddJsonBody(new { faces = from f in FacesIds select new { face_id = f } });

            var response = await client.ExecutePostAsync<FaceGet>(request);

            Debug.WriteLine(response.Content);

            if (response.ErrorException != null)
            {
                Error?.Invoke(null, new CorticaErrorEventArgs() { Error = response.ErrorException, Message = $"{MethodBase.GetCurrentMethod().ReflectedType.Name}: {response.ErrorMessage}" });
            }
            return response.Data;
        }

        /// <summary>
        /// Detect faces in images
        /// </summary>
        /// <param name="ServerAddress">[http]|[https]://[IP address]:[Port]</param>
        /// <param name="Images">Images in which to detect faces</param>
        /// <param name="GetCrops">Flag indicating whether to return face crop images or only bounding boxes. ​Default:​ True.</param>
        /// <returns>Return list of detected face bounding boxes per image, and face crops (if get_crops=True)</returns>
        public static async Task<FaceDetect> DetectFaces(string ServerAddress, dynamic AccessToken, IEnumerable<string> Images, bool GetCrops = true)
        {
            var client = new RestClient(ServerAddress);
            client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
            client.UseSystemTextJson(new JsonSerializerOptions() { IgnoreNullValues = true, PropertyNameCaseInsensitive = true });
            var request = new RestRequest("poi_db/face/detect/", Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("authorization", "Bearer " + AccessToken);
            request.AddJsonBody(new { imgs = from i in Images select new { img = i }, get_crops = GetCrops });

            var response = await client.ExecutePostAsync<FaceDetect>(request);

            Debug.WriteLine(response.Content);

            if (response.ErrorException != null)
            {
                Error?.Invoke(null, new CorticaErrorEventArgs() { Error = response.ErrorException, Message = $"{MethodBase.GetCurrentMethod().ReflectedType.Name}: {response.ErrorMessage}" });
            }
            return response.Data;
        }

        /// <summary>
        /// Search POIs by face image - looks for faces belonging to same person as input face
        /// </summary>
        /// <param name="ServerAddress">[http]|[https]://[IP address]:[Port]</param>
        /// <param name="Image">Base64 encoded string of a jpeg image of a face</param>
        /// <param name="WatchLists">Restrict the search to these watchlists</param>
        /// <param name="MinConfidence">Minimum confidence of the returned matches</param>
        /// <param name="MaxMatches">Maximum number of matches returned by the DB</param>
        /// <param name="GetCrops">Return matched crops. ​Default​: False</param>
        /// <returns>Return list of matches and confidence</returns>
        public static async Task<FaceSearch> SearchFace(string ServerAddress, dynamic AccessToken, string Image, IEnumerable<string> WatchLists, double MinConfidence, int MaxMatches, bool GetCrops = false)
        {
            var client = new RestClient(ServerAddress);
            client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
            client.UseSystemTextJson(new JsonSerializerOptions() { IgnoreNullValues = true, PropertyNameCaseInsensitive = true });
            var request = new RestRequest("poi_db/face/search/", Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("authorization", "Bearer " + AccessToken);
            request.AddJsonBody(new { img = Image, watchlists = from w in WatchLists select new { watchlist_id = w }, min_confidence = MinConfidence, max_matches = MaxMatches, get_crops = GetCrops });

            var response = await client.ExecutePostAsync<FaceSearch>(request);

            Debug.WriteLine(response.Content);

            if (response.ErrorException != null)
            {
                Error?.Invoke(null, new CorticaErrorEventArgs() { Error = response.ErrorException, Message = $"{MethodBase.GetCurrentMethod().ReflectedType.Name}: {response.ErrorMessage}" });
            }
            return response.Data;
        }

        /// <summary>
        /// Performs one to one comparison of the primary images contained within each of a reference frame and a test frame.
        /// <br>Returns an integer between 1 and 100 indicating the confidence that the reference face is contained within the test frame.</br>
        /// <br>Both reference image and test image must contain a face for the comparison to be correctly executed.</br>
        /// <br>If either image contains more than one face, the largest face within the image is assumed to be the reference/test face.</br>
        /// </summary>
        /// <param name="ServerAddress">[http]|[https]://[IP address]:[Port]</param>
        /// <param name="ReferenceImage">Base64 encoded string of a jpeg image containing a face -- this image is the “known” face</param>
        /// <param name="TestImage">Base64 encoded string of a jpeg image containing a face -- this image is the “unknown” face</param>
        /// <param name="AllowMultiFaceImages">If True, then the largest face crop in each image is chosen for comparison.
        /// If False, the request fails with 400 if either image is found to include multiple faces. ​Default: ​False</param>
        /// <returns></returns>
        public static async Task<FaceCompare> CompareFace(string ServerAddress, dynamic AccessToken, string ReferenceImage, string TestImage, bool AllowMultiFaceImages = false)
        {
            var client = new RestClient(ServerAddress);
            client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
            client.UseSystemTextJson(new JsonSerializerOptions() { IgnoreNullValues = true, PropertyNameCaseInsensitive = true });
            var request = new RestRequest("poi_db/face/compare/", Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("authorization", "Bearer " + AccessToken);
            request.AddJsonBody(new { img_ref = ReferenceImage, img_test = TestImage, allow_multiface_images = AllowMultiFaceImages });

            var response = await client.ExecutePostAsync<FaceCompare>(request);

            Debug.WriteLine(response.Content);

            if (response.ErrorException != null)
            {
                Error?.Invoke(null, new CorticaErrorEventArgs() { Error = response.ErrorException, Message = $"{MethodBase.GetCurrentMethod().ReflectedType.Name}: {response.ErrorMessage}" });
            }
            return response.Data;
        }
        #endregion

        #region Cameras
        /// <summary>
        /// Returns camera_ids of all Cameras in the system.
        /// </summary>
        /// <param name="ServerAddress">[http]|[https]://[IP address]:[Port]</param>
        /// <returns></returns>
        public async Task<CamerasGet> GetCameras(string ServerAddress, dynamic AccessToken)
        {
            try
            {
                var client = new RestClient(ServerAddress);
                client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
                client.UseSystemTextJson(new JsonSerializerOptions() { IgnoreNullValues = true, PropertyNameCaseInsensitive = true });
                var request = new RestRequest("cameras/", Method.GET);
                request.AddHeader("content-type", "application/json");
                request.AddHeader("authorization", "Bearer " + AccessToken);
                
                var response = await client.ExecuteGetAsync<CamerasGet>(request);

                if (response.ErrorException != null)
                    Error?.Invoke(null, new CorticaErrorEventArgs() { Error = response.ErrorException, Message = $"{MethodBase.GetCurrentMethod().ReflectedType.Name}: {response.ErrorMessage}" });

                Online = true;
                LastComms = DateTime.Now;

                return response.Data;
            }
            catch(Exception ex)
            {
                Online = false;

                Error?.Invoke(this, new CorticaErrorEventArgs() { Message = $"Error in {MethodBase.GetCurrentMethod().ReflectedType.Name} for {this.ServerName}: {ex.Message}", Error = ex });
                return null;
            }
        }

        /// <summary>
        /// Returns the configuration of this Camera (such as capture_address, default_stream_config), as well as its current status (if a stream exists for this camera, and its status)
        /// </summary>
        /// <param name="ServerAddress">[http]|[https]://[IP address]:[Port]</param>
        /// <param name="CameraId">Unique identifier of the Camera to get</param>
        /// <returns></returns>
        public static async Task<CameraGet> GetCamera(string ServerAddress, string CameraId, dynamic AccessToken)
        {
            var client = new RestClient(ServerAddress);
            client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
            client.UseSystemTextJson(new JsonSerializerOptions() { IgnoreNullValues = true, PropertyNameCaseInsensitive = true });
            var request = new RestRequest("cameras /{ camera_id }", Method.GET);
            request.AddHeader("Content-Type", "application/json");
            request.AddHeader("authorization", "Bearer " + AccessToken);
            request.AddUrlSegment("camera_id", CameraId);

            var response = await client.ExecuteGetAsync<CameraGet>(request);

            Debug.WriteLine(response.Content);

            if (response.ErrorException != null)
                Error?.Invoke(null, new CorticaErrorEventArgs() { Error = response.ErrorException, Message = $"{MethodBase.GetCurrentMethod().ReflectedType.Name}: {response.ErrorMessage}" });

            return response.Data;
        }
        #endregion
    }
}
