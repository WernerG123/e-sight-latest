﻿using CorticaIntegration.CorticaData.Event;
using System;
using System.Collections.Generic;
using System.Text;

namespace CorticaIntegration.EventArguments
{
    public class CorticaEventArgs : EventArgs
    {
        public EventData Event_data { get; set; }
    }
}
