﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CorticaIntegration.EventArguments
{
    public class CorticaErrorEventArgs : EventArgs
    {
        public string Message { get; set; }
        public Exception Error { get; set; }
    }
}
