﻿using System.Collections.Generic;

namespace CorticaIntegration.Data.Watchlist
{
    public class Watchlist
    {
        private double threshold_delta;

        private string watchlist_type;

        private string display_name;

        private string display_color;

        private string watchlist_id;

        private List<Poi> pois;

        public double Threshold_delta { get => threshold_delta; set => threshold_delta = value; }
        public string Watchlist_type { get => watchlist_type; set => watchlist_type = value; }
        public string Display_name { get => display_name; set => display_name = value; }
        public string Display_color { get => display_color; set => display_color = value; }
        public string Watchlist_id { get => watchlist_id; set => watchlist_id = value; }
        public List<Poi> Pois { get => pois; set => pois = value; }
    }
}
