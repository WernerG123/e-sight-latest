﻿using CorticaIntegration.Data.Metadata;

namespace CorticaIntegration.Data.Watchlist
{
    public class Body
    {
        private global::CorticaIntegration.Data.Metadata.Metadata metadata;

        private Watchlist data;

        public Metadata.Metadata Metadata { get => metadata; set => metadata = value; }
        public Watchlist Data { get => data; set => data = value; }
    }
}
