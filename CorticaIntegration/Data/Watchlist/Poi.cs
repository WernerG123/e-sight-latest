﻿namespace CorticaIntegration.Data.Watchlist
{
    public class Poi
    {
        private string poi_id;

        private string display_name;

        public string Poi_id { get => poi_id; set => poi_id = value; }
        public string Display_name { get => display_name; set => display_name = value; }
    }
}
