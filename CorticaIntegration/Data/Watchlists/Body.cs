﻿using CorticaIntegration.Data.Metadata;

namespace CorticaIntegration.Data.Watchlists
{
    public class Body
    {
        private global::CorticaIntegration.Data.Metadata.Metadata metadata;

        private Data data;

        public Metadata.Metadata Metadata { get => metadata; set => metadata = value; }
        public Data Data { get => data; set => data = value; }
    }
}
