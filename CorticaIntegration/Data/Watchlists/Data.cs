﻿using System.Collections.Generic;

namespace CorticaIntegration.Data.Watchlists
{
    public class Data
    {
        private List<Watchlist> watchlists;

        public List<Watchlist> Watchlists { get => watchlists; set => watchlists = value; }
    }
}
