﻿namespace CorticaIntegration.Data.Watchlists
{
    public class Watchlist
    {
        private string display_name;

        private string watchlist_id;

        public string Display_name { get => display_name; set => display_name = value; }
        public string Watchlist_id { get => watchlist_id; set => watchlist_id = value; }
    }
}
