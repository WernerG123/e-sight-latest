﻿namespace CorticaIntegration.Data.Metadata
{
    public class Metadata
    {
        private string msg;

        public string Msg { get => msg; set => msg = value; }
    }
}
