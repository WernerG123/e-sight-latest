﻿namespace CorticaIntegration.Data.Event
{
    public class Event_data
    {
        private string event_id;

        private string event_type;

        private Camera_data camera_data;

        private Frame_data frame_data;

        private Appearance_data appearance_data;

        private Crop_data crop_data;

        private Match_data match_data;

        public string Event_type { get => event_type; set => event_type = value; }
        public string Event_id { get => event_id; set => event_id = value; }
        public Frame_data Frame_data { get => frame_data; set => frame_data = value; }
        public Appearance_data Appearance_data { get => appearance_data; set => appearance_data = value; }
        public Crop_data Crop_data { get => crop_data; set => crop_data = value; }
        public Match_data Match_data { get => match_data; set => match_data = value; }
        public Camera_data Camera_data { get => camera_data; set => camera_data = value; }
    }
}
