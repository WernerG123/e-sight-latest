﻿namespace CorticaIntegration.Data.Event
{
    public class Crop_data
    {
        private string face_crop_img;

        private bool update_face_crop;

        private double face_score;

        public string Face_crop_img { get => face_crop_img; set => face_crop_img = value; }
        public bool Update_face_crop { get => update_face_crop; set => update_face_crop = value; }
        public double Face_score { get => face_score; set => face_score = value; }
    }
}
