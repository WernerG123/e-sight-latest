﻿namespace CorticaIntegration.Data.Event
{
    public class Bounding_box
    {
        private int x1;

        private int y1;

        private int x2;

        private int y2;

        public int X1 { get => x1; set => x1 = value; }
        public int Y1 { get => y1; set => y1 = value; }
        public int X2 { get => x2; set => x2 = value; }
        public int Y2 { get => y2; set => y2 = value; }
    }
}
