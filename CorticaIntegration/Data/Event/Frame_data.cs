﻿namespace CorticaIntegration.Data.Event
{
    public class Frame_data
    {
        private double utc_time_recorded;

        private int utc_time_zone;

        private int frame_id;

        private Bounding_box bounding_box;

        public double Utc_time_recorded { get => utc_time_recorded; set => utc_time_recorded = value; }
        public int Utc_time_zone { get => utc_time_zone; set => utc_time_zone = value; }
        public int Frame_id { get => frame_id; set => frame_id = value; }
        public Bounding_box Bounding_box { get => bounding_box; set => bounding_box = value; }
    }
}
