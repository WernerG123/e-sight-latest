﻿using System.Collections.Generic;

namespace CorticaIntegration.Data.Event
{
    public class Match_data
    {
        private string person_id;

        private string poi_id;

        private double? poi_confidence;

        private string poi_display_img;

        private string poi_display_name;

        private List<Watchlist> watchlists;

        public string Person_id { get => person_id; set => person_id = value; }
        public string Poi_id { get => poi_id; set => poi_id = value; }
        public double? Poi_confidence { get => poi_confidence; set => poi_confidence = value; }
        public string Poi_display_img { get => poi_display_img; set => poi_display_img = value; }
        public string Poi_display_name { get => poi_display_name; set => poi_display_name = value; }
        public List<Watchlist> Watchlists { get => watchlists; set => watchlists = value; }
    }
}
