﻿namespace CorticaIntegration.Data.Event
{
    public class Watchlist
    {
        private string watchlist_id;

        private string watchlist_type;

        private string match_outcome;

        private string display_name;

        private string display_color;

        public string Watchlist_id { get => watchlist_id; set => watchlist_id = value; }
        public string Watchlist_type { get => watchlist_type; set => watchlist_type = value; }
        public string Match_outcome { get => match_outcome; set => match_outcome = value; }
        public string Display_name { get => display_name; set => display_name = value; }
        public string Display_color { get => display_color; set => display_color = value; }
    }
}
