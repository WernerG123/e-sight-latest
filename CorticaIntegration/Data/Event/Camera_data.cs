﻿namespace CorticaIntegration.Data.Event
{
    public class Camera_data
    {
        private string camera_id;

        private string stream_id;

        private string camera_description;

        public string Camera_id { get => camera_id; set => camera_id = value; }
        public string Stream_id { get => stream_id; set => stream_id = value; }
        public string Camera_description { get => camera_description; set => camera_description = value; }
    }
}
