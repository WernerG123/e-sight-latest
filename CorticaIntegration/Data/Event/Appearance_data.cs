﻿namespace CorticaIntegration.Data.Event
{    
    public class Appearance_data
    {        
        private string appearance_id;

        private double utc_time_started;

        private int? first_frame_id;

        private bool is_last_appearance;

        public string Appearance_id { get => appearance_id; set => appearance_id = value; }
        public double Utc_time_started { get => utc_time_started; set => utc_time_started = value; }
        public int? First_frame_id { get => first_frame_id; set => first_frame_id = value; }
        public bool Is_last_appearance { get => is_last_appearance; set => is_last_appearance = value; }
    }
}
