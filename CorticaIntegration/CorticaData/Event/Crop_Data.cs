﻿using System.Text.Json.Serialization;

namespace CorticaIntegration.CorticaData.Event
{
    public class Crop_Data
    {
        public string Face_crop_img { get; set; }
        public bool Update_face_crop { get; set; }
        public double Face_score { get; set; }
        public float? Pitch { get; set; }
        public float? Yaw { get; set; }
    }

}
