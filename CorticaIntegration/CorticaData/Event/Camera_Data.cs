﻿namespace CorticaIntegration.CorticaData.Event
{
    public class Camera_Data
    {
        public string Camera_id { get; set; }
        public string Stream_id { get; set; }
        public string Camera_description { get; set; }
    }

}
