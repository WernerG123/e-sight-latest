﻿namespace CorticaIntegration.CorticaData.Event
{
    public class Appearance_Data
    {
        public string Appearance_id { get; set; }
        public float Utc_time_started { get; set; }
        public int? First_frame_id { get; set; }
        public bool Is_last_appearance { get; set; }
    }

}
