﻿namespace CorticaIntegration.CorticaData.Event
{
    public class Bounding_Box
    {
        public int X1 { get; set; }
        public int Y1 { get; set; }
        public int X2 { get; set; }
        public int Y2 { get; set; }
    }

}
