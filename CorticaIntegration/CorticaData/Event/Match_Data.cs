﻿using System.Collections.Generic;

namespace CorticaIntegration.CorticaData.Event
{
    public class Match_Data
    {
        public string Person_id { get; set; }
        public string Poi_id { get; set; }
        public double? Poi_confidence { get; set; }
        public string Poi_display_img { get; set; }
        public string Poi_display_name { get; set; }
        public List<Watchlist> Watchlists { get; set; }
    }

}
