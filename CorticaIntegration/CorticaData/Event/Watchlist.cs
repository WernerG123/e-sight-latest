﻿namespace CorticaIntegration.CorticaData.Event
{
    public class Watchlist
    {
        public string Watchlist_id { get; set; }
        public string Watchlist_type { get; set; }
        public string Match_outcome { get; set; }
        public string Display_name { get; set; }
        public string Display_color { get; set; }
    }

}
