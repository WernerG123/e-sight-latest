﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CorticaIntegration.CorticaData.Event
{
    public class EventData
    {
        public string Event_id { get; set; }
        public string Event_type { get; set; }
        public Camera_Data Camera_data { get; set; }
        public Frame_Data Frame_data { get; set; }
        public Appearance_Data Appearance_data { get; set; }
        public Crop_Data Crop_data { get; set; }
        public Match_Data Match_data { get; set; }
    }
}
