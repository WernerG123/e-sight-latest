﻿namespace CorticaIntegration.CorticaData.Event
{
    public class Frame_Data
    {
        public float Utc_time_recorded { get; set; }
        public int Utc_time_zone { get; set; }
        public int Frame_id { get; set; }
        public Bounding_Box Bounding_box { get; set; }
    }

}
