﻿namespace CorticaIntegration.CorticaData.Shared.Metadata
{
    public class MetaDataSuccessMessage
    {
        public bool Success { get; set; }
        public string Msg { get; set; }
    }
}