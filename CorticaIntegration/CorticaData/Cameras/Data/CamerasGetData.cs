﻿using CorticaIntegration.CorticaData.Cameras.Shared;
using CorticaIntegration.CorticaData.Cameras.Summary;

namespace CorticaIntegration.CorticaData.Cameras.Data
{
    public class CamerasGetData
    {
        public Camera[] Cameras { get; set; }
        public CamerasGetSummary Summary { get; set; }
    }
}
