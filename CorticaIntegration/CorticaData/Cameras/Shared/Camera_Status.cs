﻿namespace CorticaIntegration.CorticaData.Cameras.Shared
{
    public class Camera_Status
    {
        public int Status { get; set; }
        public string Status_str { get; set; }
        public float Time_active { get; set; }
        public int Elapsed_frames { get; set; }
        public float Elapsed_progress { get; set; }
    }
}