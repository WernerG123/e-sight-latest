﻿namespace CorticaIntegration.CorticaData.Cameras.Shared
{
    public class Camera
    {
        public string Description { get; set; }
        public string Display_rtsp_address { get; set; }
        public bool Persist { get; set; }
        public Camera_Status Camera_status { get; set; }
        public Config Config { get; set; }
        public string Camera_id { get; set; }
        public bool Analyze { get; set; }
        public string Mode { get; set; }
        public string Capture_address { get; set; }
        public int In_fps { get; set; }
        public int Capture_port { get; set; }
        public int Rebroadcast_frame_width { get; set; }
        public int Min_detection_width { get; set; }
        public Video_Settings Video_settings { get; set; }
    }
}
