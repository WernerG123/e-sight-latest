﻿using CorticaIntegration.CorticaData.Shared;

namespace CorticaIntegration.CorticaData.Cameras.Shared
{
    public class Config
    {
        public float Face_recognition_threshold { get; set; }
        public WatchlistBasic[] Watchlists { get; set; }
        public Events_Outputs[] Events_outputs { get; set; }
        public string Cameradescription { get; set; }
        public string Analysis_quality { get; set; }
        public bool Is_live { get; set; }
    }
}