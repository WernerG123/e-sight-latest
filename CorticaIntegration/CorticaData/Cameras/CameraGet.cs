﻿using CorticaIntegration.CorticaData.Cameras.Shared;
using CorticaIntegration.CorticaData.Shared.Metadata;

namespace CorticaIntegration.CorticaData.Cameras
{
    public class CameraGet
    {
        public MetadataBasic Metadata { get; set; }
        public Camera Data { get; set; }
    }
}