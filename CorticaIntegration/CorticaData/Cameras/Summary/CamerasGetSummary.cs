﻿namespace CorticaIntegration.CorticaData.Cameras.Summary
{
    public class CamerasGetSummary
    {
        public int Total_cameras { get; set; }
        public int Active_cameras { get; set; }
        public int Irregular_state_cameras { get; set; }
    }
}
