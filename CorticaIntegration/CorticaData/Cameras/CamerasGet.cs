﻿using CorticaIntegration.CorticaData.Cameras.Data;
using CorticaIntegration.CorticaData.Shared.Metadata;

namespace CorticaIntegration.CorticaData.Cameras
{
    public class CamerasGet
    {
        public MetadataBasic Metadata { get; set; }
        public CamerasGetData Data { get; set; }
    }
}