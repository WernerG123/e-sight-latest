﻿using CorticaIntegration.CorticaData.PoiDb.Data;
using CorticaIntegration.CorticaData.Shared.Metadata;

namespace CorticaIntegration.CorticaData.PoiDb.Shared
{
    public class WatchlistGet
    {
        public MetadataBasic Metadata { get; set; }
        public WatchlistGetData Data { get; set; }
    }
}
