﻿using CorticaIntegration.CorticaData.PoiDb.Data;
using CorticaIntegration.CorticaData.Shared.Metadata;

namespace CorticaIntegration.CorticaData.PoiDb.Shared
{
    public class FaceGet
    {
        public MetadataList Metadata { get; set; }
        public FaceGetData Data { get; set; }
    }
}
