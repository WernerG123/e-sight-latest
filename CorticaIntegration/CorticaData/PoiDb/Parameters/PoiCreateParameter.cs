﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CorticaIntegration.CorticaData.PoiDb.Parameters
{
    public class PoiCreateParameter
    {
        public string Poi_Id { get; set; }
        public string Display_name { get; set; }
        public string Display_img { get; set; }
        public bool Save_face { get; set; } = true;
        public bool Use_as_face { get; set; } = true;
    }
}
