﻿namespace CorticaIntegration.CorticaData.PoiDb.Parameters
{
    public class WatchlistCreateParameter
    {
        public int Threshold_delta { get; set; }
        public string Watchlist_type { get; set; }
        public string Display_name { get; set; }
        public string Display_color { get; set; }
    }
}
