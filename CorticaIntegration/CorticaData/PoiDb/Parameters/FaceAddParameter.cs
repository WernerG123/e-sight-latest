﻿namespace CorticaIntegration.CorticaData.PoiDb.Parameters
{
    public class FaceAddParameter
    {
        public string Img { get; set; }
        public string Poi_id { get; set; }
        public bool Save_crop { get; set; }
    }
}
