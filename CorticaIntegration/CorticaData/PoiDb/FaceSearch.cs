﻿using CorticaIntegration.CorticaData.PoiDb.Data;
using CorticaIntegration.CorticaData.Shared.Metadata;

namespace CorticaIntegration.CorticaData.PoiDb
{
    public class FaceSearch
    {
        public MetadataBasic Metadata { get; set; }
        public SearchData Data { get; set; }
}
}
