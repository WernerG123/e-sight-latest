﻿using CorticaIntegration.CorticaData.PoiDb.Data;
using CorticaIntegration.CorticaData.PoiDb.Metadata;

namespace CorticaIntegration.CorticaData.PoiDb.Shared
{
    public class PoiRemove
    {
        public MetadataPoiList Metadata { get; set; }
        public EmptyData Data { get; set; }
    }
}
