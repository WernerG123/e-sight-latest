﻿using CorticaIntegration.CorticaData.PoiDb.Shared;

namespace CorticaIntegration.CorticaData.PoiDb.Data
{
    public class PoiGetData
    {
        public PoiDetail[] Pois { get; set; }
    }
}
