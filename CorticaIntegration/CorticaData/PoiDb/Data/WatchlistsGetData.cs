﻿using CorticaIntegration.CorticaData.PoiDb.Shared;

namespace CorticaIntegration.CorticaData.PoiDb.Data
{
    public class WatchlistsGetData
    {
        public Watchlist[] Watchlists { get; set; }
    }
}
