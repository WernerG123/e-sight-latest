﻿using CorticaIntegration.CorticaData.PoiDb.Shared;

namespace CorticaIntegration.CorticaData.PoiDb.Data
{
    public class DetectionData
    {
        public Detection[][] Detections { get; set; }
    }
}
