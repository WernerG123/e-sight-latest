﻿using CorticaIntegration.CorticaData.PoiDb.Shared;

namespace CorticaIntegration.CorticaData.PoiDb.Data
{
    public class FaceAddData
    {
        public FaceBasic[] Faces { get; set; }
    }
}
