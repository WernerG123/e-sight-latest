﻿namespace CorticaIntegration.CorticaData.PoiDb.Data
{
    public class FaceCompareData
    {
        public float Match_confidence { get; set; }
    }
}