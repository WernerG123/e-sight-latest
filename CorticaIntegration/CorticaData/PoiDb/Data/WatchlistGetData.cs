﻿using CorticaIntegration.CorticaData.PoiDb.Shared;

namespace CorticaIntegration.CorticaData.PoiDb.Data
{
    public class WatchlistGetData : WatchlistData
    {
        public Poi[] Pois { get; set; }
    }
}
