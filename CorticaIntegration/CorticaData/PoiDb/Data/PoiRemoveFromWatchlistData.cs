﻿namespace CorticaIntegration.CorticaData.PoiDb.Data
{
    public class PoiRemoveFromWatchlistData
    {
        public string Watchlist_id { get; set; }
        public string[] Poi_ids { get; set; }
    }
}
