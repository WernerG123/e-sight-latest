﻿using CorticaIntegration.CorticaData.PoiDb.Shared;

namespace CorticaIntegration.CorticaData.PoiDb.Data
{
    public class WatchlistData : Watchlist
    {        
        public float Threshold_delta { get; set; }
        public string Watchlist_type { get; set; }
        public string Display_color { get; set; }
    }
}
