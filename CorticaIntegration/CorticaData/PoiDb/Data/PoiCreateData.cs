﻿using CorticaIntegration.CorticaData.PoiDb.Shared;

namespace CorticaIntegration.CorticaData.PoiDb.Data
{
    public class PoiCreateData
    {
        public Poi[] Pois { get; set; }
        public Watchlist[] Watchlists { get; set; }
    }
}
