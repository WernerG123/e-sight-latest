﻿using CorticaIntegration.CorticaData.PoiDb.Shared;

namespace CorticaIntegration.CorticaData.PoiDb.Data
{
    public class PoisGetData
    {
        public Poi[] Pois { get; set; }
    }
}
