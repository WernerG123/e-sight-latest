﻿using CorticaIntegration.CorticaData.PoiDb.Shared;

namespace CorticaIntegration.CorticaData.PoiDb.Data
{
    public class SearchData
    {
        public Match[] Matches { get; set; }
    }
}
