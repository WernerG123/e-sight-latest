﻿using CorticaIntegration.CorticaData.PoiDb.Shared;

namespace CorticaIntegration.CorticaData.PoiDb.Data
{
    public class FaceGetData
    {
        public FaceOfPoi[] Faces { get; set; }
    }
}
