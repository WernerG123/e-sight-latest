﻿using CorticaIntegration.CorticaData.PoiDb.Data;
using CorticaIntegration.CorticaData.Shared.Metadata;

namespace CorticaIntegration.CorticaData.PoiDb.Shared
{
    public class WatchlistRemove
    {
        public MetadataBasic Metadata { get; set; }
        public WatchlistData Data { get; set; }
    }
}
