﻿using CorticaIntegration.CorticaData.PoiDb.Data;
using CorticaIntegration.CorticaData.Shared.Metadata;

namespace CorticaIntegration.CorticaData.PoiDb.Shared
{
    public class WatchlistsGet
    {
        public MetadataBasic Metadata { get; set; }
        public WatchlistsGetData Data { get; set; }
    }
}
