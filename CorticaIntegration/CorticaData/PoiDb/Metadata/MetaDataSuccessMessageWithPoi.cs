﻿using CorticaIntegration.CorticaData.Shared.Metadata;

namespace CorticaIntegration.CorticaData.PoiDb.Metadata
{
    public class MetaDataSuccessMessageWithPoi : MetaDataSuccessMessage
    {
        public string Poi_id { get; set; }
    }
}