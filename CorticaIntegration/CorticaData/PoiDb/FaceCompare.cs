﻿using CorticaIntegration.CorticaData.PoiDb.Data;
using CorticaIntegration.CorticaData.Shared.Metadata;

namespace CorticaIntegration.CorticaData.PoiDb
{
    public class FaceCompare
    {
        public MetadataBasic Metadata { get; set; }
        public FaceCompareData Data { get; set; }
    }
}