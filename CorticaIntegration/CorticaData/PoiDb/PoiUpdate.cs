﻿using CorticaIntegration.CorticaData.PoiDb.Data;
using CorticaIntegration.CorticaData.Shared.Metadata;

namespace CorticaIntegration.CorticaData.PoiDb.Shared
{
    public class PoiUpdate
    {
        public MetadataBasic Metadata { get; set; }
        public MetaDataSuccessMessage Message { get; set; }
        public EmptyData Data { get; set; }
    }
}