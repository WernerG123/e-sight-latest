﻿using CorticaIntegration.CorticaData.Shared;

namespace CorticaIntegration.CorticaData.PoiDb.Shared
{
    public class Watchlist : WatchlistBasic
    {
        public string Display_name { get; set; }
    }
}
