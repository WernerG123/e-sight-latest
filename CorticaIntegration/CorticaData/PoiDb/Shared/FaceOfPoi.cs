﻿namespace CorticaIntegration.CorticaData.PoiDb.Shared
{
    public class FaceOfPoi : Face
    {
        public string Poi_id { get; set; }
    }
}
