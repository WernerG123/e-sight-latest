﻿namespace CorticaIntegration.CorticaData.PoiDb.Shared
{
    public class Detection
    {
        public float Detection_score { get; set; }
        public Bbox Bbox { get; set; }
        public string Img { get; set; }
    }
}
