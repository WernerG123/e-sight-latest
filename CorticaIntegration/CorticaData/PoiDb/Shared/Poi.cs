﻿namespace CorticaIntegration.CorticaData.PoiDb.Shared
{
    public class Poi
    {
        public string Display_name { get; set; }
        public string Poi_id { get; set; }
    }
}
