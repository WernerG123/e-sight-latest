﻿using CorticaIntegration.CorticaData.Shared;

namespace CorticaIntegration.CorticaData.PoiDb.Shared
{
    public class PoiDetail : Poi
    {
        public string Display_img { get; set; }
        public Face[] Faces { get; set; }
        public WatchlistBasic[] Watchlists { get; set; }
    }
}
