﻿namespace CorticaIntegration.CorticaData.PoiDb.Shared
{
    public class FaceSearchResult : FaceOfPoi
    {
        public float Face_score { get; set; }
        public float Confidence { get; set; }
    }
}
