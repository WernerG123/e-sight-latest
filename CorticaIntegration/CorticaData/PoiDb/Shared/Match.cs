﻿using CorticaIntegration.CorticaData.PoiDb.Data;

namespace CorticaIntegration.CorticaData.PoiDb.Shared
{
    public class Match
    {
        public string Display_img { get; set; }
        public string Display_name { get; set; }
        public FaceSearchResult[] Faces { get; set; }
        public string Poi_id { get; set; }
        public float Poi_confidence { get; set; }
        public WatchlistData[] Watchlists { get; set; }
    }
}
