﻿using CorticaIntegration.CorticaData.PoiDb.Data;
using CorticaIntegration.CorticaData.PoiDb.Metadata;

namespace CorticaIntegration.CorticaData.PoiDb.Shared
{
    public class PoiRemoveFromWatchlist
    {
        public MetadataPoiList Metadata { get; set; }
        public PoiRemoveFromWatchlistData Data { get; set; }
    }

}
