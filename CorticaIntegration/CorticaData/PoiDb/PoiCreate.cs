﻿using CorticaIntegration.CorticaData.PoiDb.Data;
using CorticaIntegration.CorticaData.Shared.Metadata;

namespace CorticaIntegration.CorticaData.PoiDb.Shared
{
    public class PoiCreate
    {
        public MetadataList Metadata { get; set; }
        public PoiCreateData Data { get; set; }
    }
}
