﻿using CorticaIntegration.CorticaData.PoiDb.Data;
using CorticaIntegration.CorticaData.Shared.Metadata;

namespace CorticaIntegration.CorticaData.PoiDb
{
    public class FaceAdd
    {
        public MetadataList Metadata { get; set; }
        public FaceAddData Data { get; set; }
    }
}
