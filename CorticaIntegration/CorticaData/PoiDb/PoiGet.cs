﻿using CorticaIntegration.CorticaData.PoiDb.Data;
using CorticaIntegration.CorticaData.Shared.Metadata;

namespace CorticaIntegration.CorticaData.PoiDb
{
    public class PoiGet
    {
        public MetadataList Metadata { get; set; }
        public PoiGetData Data { get; set; }
    }
}
