﻿using CorticaIntegration.CorticaData.PoiDb.Data;
using CorticaIntegration.CorticaData.Shared.Metadata;

namespace CorticaIntegration.CorticaData.PoiDb
{
    public class PoisGet
    {
        public MetadataBasic Metadata { get; set; }
        public PoisGetData Data { get; set; }
    }
}
