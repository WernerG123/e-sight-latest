﻿using CorticaIntegration.CorticaData.PoiDb.Data;
using CorticaIntegration.CorticaData.Shared.Metadata;

namespace CorticaIntegration.CorticaData.PoiDb.Shared
{
    public class FaceRemove
    {
        public MetadataList Metadata { get; set; }
        public EmptyData Data { get; set; }
    }
}
