﻿using CorticaIntegration.CorticaData.PoiDb.Data;
using CorticaIntegration.CorticaData.Shared.Metadata;

namespace CorticaIntegration.CorticaData.PoiDb
{
    public class FaceDetect
    {
        public MetadataList Metadata { get; set; }
        public DetectionData Data { get; set; }
    }
}
