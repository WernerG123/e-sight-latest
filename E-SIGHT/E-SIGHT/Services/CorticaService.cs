﻿using CorticaIntegration;
using E_SIGHT.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using System.Data.Common;
using System.Runtime.InteropServices;
using CorticaIntegration.CorticaData.Cameras.Shared;
using CorticaIntegration.CorticaData.Event;
using CorticaIntegration.CorticaData.PoiDb.Shared;
using CorticaIntegration.EventArguments;
using E_SIGHT.EventArguments;
using CorticaIntegration.CorticaData.PoiDb.Parameters;
using CorticaIntegration.CorticaData.PoiDb;
using System.Net.Http;
using Microsoft.Extensions.Options;
using RestSharp;
using Newtonsoft.Json.Linq;
using RestSharp.Serializers.SystemTextJson;
using System.Text.Json;

namespace E_SIGHT.Services
{
    public class CorticaService
    {
        public IList<Cortica> CorticaServers { get; set; }

        public readonly List<FaceDetectedEvent> faceEvents;    
        public List<FaceDetectedEvent> oldFaceEvents;
        public List<FaceDetectedEvent> currentFaceEvents;

        private bool pauseFeed { get; set; }

        public IEnumerable<FaceDetectedEvent> FaceEvents
        {
            get
            {
               
                    oldFaceEvents = faceEvents.ToList();
                    return faceEvents.Where<FaceDetectedEvent>((e) => (EventFilterCameras.FirstOrDefault((cam) => cam.Id == e.CameraId).CheckedVal)
                                                                    && ((EventFilterWatchlistAll && !e.IsMatch) || e.Watchlists.Select((w) => w.Watchlist_id).Intersect(EventFilterWatchlist.Where((wl) => wl.CheckedVal).Select((wl) => wl.Id)).Count() > 0)
                                                                    && (DateTime.Now - e.AppearanceDateTime.ToLocalTime()).TotalMinutes <= EventFilterTime);
              
            }
        }

        public IList<EventFilterObject> EventFilterCameras { get; set; } = new List<EventFilterObject>();
        public IList<EventFilterObject> EventFilterWatchlist { get; set; } = new List<EventFilterObject>();
        public IList<EventFilterObject> PoiEnrollmentWatchlist { get; set; } = new List<EventFilterObject>();
        public IList<EventFilterObject> FaceSearchWatchlist { get; set; } = new List<EventFilterObject>();

        public bool EventFilterCameraAll { get; set; } = true;
        public bool EventFilterWatchlistAll { get; set; } = true;
        public int EventFilterTime { get; set; } = int.MaxValue;

        public IList<Camera> Cameras { get; private set; } = new List<Camera>();

        public IList<CorticaIntegration.CorticaData.PoiDb.Shared.Watchlist> Watchlists { get; private set; } = new List<CorticaIntegration.CorticaData.PoiDb.Shared.Watchlist>();

        public int MatchCount { get; set; } = 0;
        public int AppearCount { get; set; } = 0;

        public event Action OnChange;
        private void NotifyDataChanged() => OnChange?.Invoke();

        public event EventHandler<FaceDetectedEventArgs> Alert;

        public event EventHandler<ErrorEventArgs> Error;

        public CorticaService(IEnumerable<ServerSettings> _ServerSettings)
        {
            CorticaServers = new List<Cortica>();
            CorticaServers = new List<Cortica>();

            if (_ServerSettings.Any(server => server.ServerName != null))
            {
                foreach (var server in _ServerSettings)
                {
                    var cortica = new Cortica() { ServerName = server.ServerName, ServerAbbrv = server.ServerAbbrv };
                    cortica.CorticaEvent += Cortica_CorticaEvent;
                    //cortica.ConnectListener(server.ListenerUrl);
                    cortica.CameraApiUrl = server.CameraApiUrl;
                    cortica.PoiApiUrl = server.PoiApiUrl;

                    CorticaServers.Add(cortica);
                }
                CorticaServices();

                faceEvents = new List<FaceDetectedEvent>();
            }
        }

        public async Task<string> GetAccessToken()
        {
            var AccessToken = await AuthService.PostAuth();
            return AccessToken;
        }

        public async Task CorticaServices()
        {
            var AccessToken = await GetAccessToken();
            await GetCameras(AccessToken);
            await GetWatchlists(AccessToken);
            
            return;
        }

        public void TogglePauseFeed(bool value)
        {
            pauseFeed = value;
        }

        public void StartListening()
        {
            StopListening();
            foreach (var server in CorticaServers)
            {
                server.ConnectListener();
            }
        }

        public void StopListening()
        {
            foreach (var server in CorticaServers)
            {
                server.DisconnectListener();
            }
        }

        public async Task GetCameras(dynamic AccessToken)
        {
            Cameras.Clear();
            EventFilterCameras.Clear();

            foreach (var server in CorticaServers)
            {
                var cams = await server.GetCameras(server.CameraApiUrl, AccessToken);

                if (cams?.Data?.Cameras != null)
                {
                    foreach (var cam in cams.Data.Cameras)
                    {
                        Cameras.Add(cam);
                        EventFilterCameras.Add(new EventFilterObject() { Id = cam.Camera_id, Description = cam.Description, ServerName = server.ServerName, ServerAbbrv = server.ServerAbbrv, CheckedVal = true });
                    }
                }
            }

            NotifyDataChanged();
        }


        public async Task GetWatchlists(dynamic AccessToken)
        {
            Watchlists.Clear();
            EventFilterWatchlist.Clear();
            PoiEnrollmentWatchlist.Clear();
            FaceSearchWatchlist.Clear();

            foreach (var server in CorticaServers)
            {
                var watchlists = await server.GetWatchlists(server.PoiApiUrl, AccessToken);

                if (watchlists?.Data?.Watchlists != null)
                {
                    foreach (var watchlist in watchlists.Data.Watchlists)
                    {
                        Watchlists.Add(watchlist);
                        EventFilterWatchlist.Add(new EventFilterObject() { Id = watchlist.Watchlist_id, Description = watchlist.Display_name, ServerName = server.ServerName, ServerAbbrv = server.ServerAbbrv, CheckedVal = true });
                        PoiEnrollmentWatchlist.Add(new EventFilterObject() { Id = watchlist.Watchlist_id, Description = watchlist.Display_name, ServerName = server.ServerName, ServerAbbrv = server.ServerAbbrv, CheckedVal = false });
                        FaceSearchWatchlist.Add(new EventFilterObject() { Id = watchlist.Watchlist_id, Description = watchlist.Display_name, ServerName = server.ServerName, ServerAbbrv = server.ServerAbbrv, CheckedVal = false });
                    }
                }
            }

            NotifyDataChanged();
        }

        public async Task<IEnumerable<PoisGet>> GetAllPois(dynamic AccessToken, IEnumerable<string> serverNames = null)
        {
            List<PoisGet> result = new List<PoisGet>();

            foreach(var server in CorticaServers.Where(s => serverNames != null && serverNames.Contains(s.PoiApiUrl) || serverNames == null))
            {
                Console.WriteLine("Getting POIs from: {0}", server.PoiApiUrl);
                result.Add(await Cortica.GetPois(server.PoiApiUrl, AccessToken));
            }
            foreach(var res in result)
            {
                Console.WriteLine(res.ToString());

            }
            return result;
        }

        public async Task<IEnumerable<PoiGet>> GetAllPoisData(dynamic AccessToken, List<string> poiIds, IEnumerable<string> serverNames = null)
        {
            List<PoiGet> result = new List<PoiGet>();

            foreach (var server in CorticaServers.Where(s => serverNames != null && serverNames.Contains(s.PoiApiUrl) || serverNames == null))
            {
                Console.WriteLine("Getting POIs from: {0}", server.PoiApiUrl);
                result.Add(await Cortica.GetPoi(server.PoiApiUrl, AccessToken, poiIds, true));
            }
            foreach (var res in result)
            {
                Console.WriteLine(res.ToString());

            }
            return result;
        }

        public async Task<PoiDetail> GetPoiData(dynamic AccessToken, List<string> poiIds, IEnumerable<string> serverNames = null)
        {
            List<PoiGet> result = new List<PoiGet>();
            PoiDetail poiToReturn = new PoiDetail();

            foreach (var server in CorticaServers.Where(s => serverNames != null && serverNames.Contains(s.PoiApiUrl) || serverNames == null))
            {
                Console.WriteLine("Getting POIs from: {0}", server.PoiApiUrl);
                result.Add(await Cortica.GetPoi(server.PoiApiUrl, AccessToken, poiIds, true));
            }

            poiToReturn = result.First().Data.Pois.First();
            
            return poiToReturn;
        }

        public async Task<IEnumerable<PoiCreate>> EnrollPois(IEnumerable<PoiCreateParameter> pois, dynamic AccessToken, IEnumerable<EventFilterObject> watchlists, IEnumerable<string> serverNames = null)
        {
            List<PoiCreate> result = new List<PoiCreate>();

            foreach (var server in CorticaServers.Where(s => serverNames != null && serverNames.Contains(s.ServerName) || serverNames == null))
            {
                var wl = watchlists.Where((w) => w.CheckedVal && w.ServerName == server.ServerName).Select((w) => w.Id).ToArray();
                result.Add(await Cortica.CreatePoi(server.PoiApiUrl, AccessToken, pois, wl));
            }

            return result;
        }

        public async Task<List<PoiUpdate>> UpdatePOI(PoiCreateParameter poi, dynamic AccessToken, IEnumerable<EventFilterObject> watchlists, IEnumerable<string> serverNames = null)
        {
            List<PoiUpdate> result = new List<PoiUpdate>();

            foreach (var server in CorticaServers.Where(s => serverNames != null && serverNames.Contains(s.ServerName) || serverNames == null))
            {
                Console.WriteLine("Adding IMG to server: " + server.ServerName);
                result.Add(await Cortica.UpdatePoi(server.PoiApiUrl, AccessToken, poi.Poi_Id, poi.Display_name, poi.Display_img));
            }

            return result;
        }

        public async Task<List<FaceAdd>> AddFace(string img, dynamic AccessToken, IEnumerable<string> serverNames = null, string poi = "")
        {
            List<FaceAdd> result = new List<FaceAdd>();
            List < FaceAddParameter > facesToAdd = new List<FaceAddParameter>();

            FaceAddParameter myFace = new FaceAddParameter();
            myFace.Poi_id = poi;
            myFace.Img = img;
            myFace.Save_crop = true;

            facesToAdd.Add(myFace);
           
            foreach (var server in CorticaServers.Where(s => serverNames != null && serverNames.Contains(s.ServerName) || serverNames == null))
            {
                Console.WriteLine("Adding IMG to server: " + server.ServerName);
                result.Add(await Cortica.AddFace(server.PoiApiUrl, AccessToken, facesToAdd));
            }

            return result;
        }


        public async Task<IEnumerable<string>> DetectFaces(string baseImg)
        {
            var result = new List<string>();
            var AccessToken = await GetAccessToken ();

            var faceDetect = await Cortica.DetectFaces(CorticaServers[0].PoiApiUrl, AccessToken, new string[] { baseImg });

            if (faceDetect?.Metadata?.Success_list != null && faceDetect.Metadata.Success_list.Length > 0 && !faceDetect.Metadata.Success_list[0].Success)
            {
                this.Error?.Invoke(this, new ErrorEventArgs() { ErrorMsg = faceDetect.Metadata.Success_list[0].Msg });
            }

            if (faceDetect?.Data?.Detections != null && faceDetect.Data.Detections.Length > 0 && faceDetect.Data.Detections[0].Length > 0)
            {
                foreach(var face in faceDetect.Data.Detections[0])
                {
                    result.Add(face.Img);
                }
            }

            return result;
        }

        public async Task<IEnumerable<FaceSearchMatch>> SearchFaceAsync(string faceImg, dynamic AccessToken, IEnumerable<EventFilterObject> watchlists, IEnumerable<string> serverNames = null, float minConfidence = 50, int maxMatchesPerServer = 10)
        {
            var result = new List<FaceSearchMatch>();

            foreach (var server in CorticaServers.Where(s => serverNames != null && serverNames.Contains(s.ServerName) || serverNames == null))
            {
                var wl = watchlists.Where((w) => w.ServerName == server.ServerName).Select((w) => w.Id).ToArray();

                var fsResult = await Cortica.SearchFace(server.PoiApiUrl, AccessToken.ToString(), faceImg, wl, minConfidence, maxMatchesPerServer);

                if(!string.IsNullOrWhiteSpace(fsResult?.Metadata?.Msg) && !fsResult.Metadata.Msg.Equals("success", StringComparison.InvariantCultureIgnoreCase))
                {
                    this.Error?.Invoke(this, new ErrorEventArgs() { ErrorMsg = fsResult.Metadata.Msg });
                }

                if (fsResult?.Data?.Matches != null)
                {
                    foreach (var match in fsResult.Data.Matches)
                    {
                        FaceSearchMatch fsMatch = new FaceSearchMatch()
                        {
                            ServerName = server.ServerName,
                            ServerAbbrv = server.ServerAbbrv,
                            PoiName = match.Display_name,
                            PoiId = match.Poi_id,
                            Img = match.Display_img,
                            Confidence = match.Poi_confidence,
                            Watchlists = match.Watchlists
                        };

                        if (string.IsNullOrWhiteSpace(fsMatch.Img) && match.Faces != null)
                        {
                            foreach(var face in match.Faces)
                            {
                                if(string.IsNullOrWhiteSpace(fsMatch.Img) || face.Confidence > fsMatch.Confidence)
                                {
                                    fsMatch.Img = face.Img;
                                    fsMatch.Confidence = face.Confidence;
                                }
                            }
                        }

                        result.Add(fsMatch);
                    }
                }
            }

            return result;
        }

        private void Cortica_CorticaEvent(object sender, CorticaIntegration.EventArguments.CorticaEventArgs e)
        {
            try
            {
                var corticaEvent = e.Event_data;

                if (corticaEvent != null)
                {
                    if (corticaEvent.Appearance_data != null)
                    {
                        DateTime frameTime = DateTime.MinValue;
                        if (corticaEvent?.Frame_data != null)
                            frameTime = DateTimeOffset.FromUnixTimeMilliseconds((long)(corticaEvent.Frame_data.Utc_time_recorded * 1000)).DateTime;

                        DateTime appearanceTime = DateTime.MinValue;
                        if (corticaEvent?.Appearance_data != null)
                            appearanceTime = DateTimeOffset.FromUnixTimeMilliseconds((long)(corticaEvent.Appearance_data.Utc_time_started * 1000)).DateTime;

                        if (corticaEvent.Appearance_data.Is_last_appearance)
                        {
                            return;
                        }

                        var server = sender as Cortica;

                        var faceEvent = new FaceDetectedEvent()
                        {
                            ServerName = server.ServerName,
                            ServerAbbrv = server.ServerAbbrv,
                            EventId = e?.Event_data?.Event_id,
                            FrameDateTime = frameTime,
                            AppearanceDateTime = appearanceTime,
                            AppearanceId = corticaEvent?.Appearance_data.Appearance_id,
                            PoiName = corticaEvent.Match_data?.Poi_display_name,
                            PoiImage = corticaEvent.Match_data?.Poi_display_img,
                            CropImage = corticaEvent.Crop_data?.Face_crop_img,
                            FaceScore = corticaEvent.Crop_data?.Face_score,
                            Confidence = corticaEvent.Match_data?.Poi_confidence,
                            CameraId = corticaEvent.Camera_data?.Camera_id,
                            CameraName = corticaEvent.Camera_data?.Camera_description,
                            Watchlists = corticaEvent.Match_data.Watchlists.Where((x) => x.Match_outcome.Equals("matched", StringComparison.InvariantCultureIgnoreCase))
                        };

                        var existingAppearance = FaceEvents.FirstOrDefault((x) => x.AppearanceId == faceEvent.AppearanceId);
                        if (existingAppearance != null)
                        {
                            existingAppearance.FrameDateTime = faceEvent.FrameDateTime;
                            existingAppearance.Tracked++;

                            if (faceEvent.FaceScore > existingAppearance.FaceScore)
                            {
                                existingAppearance.CropImage = faceEvent.CropImage;
                                existingAppearance.FaceScore = faceEvent.FaceScore;
                            }

                            if (faceEvent.Confidence > existingAppearance.Confidence)
                            {
                                existingAppearance.Confidence = faceEvent.Confidence;
                                existingAppearance.PoiName = faceEvent.PoiName;
                                existingAppearance.PoiImage = faceEvent.PoiImage;
                            }
                        }
                        else
                        {
                            faceEvents.Insert(0, faceEvent);
                            Alert?.Invoke(sender, new FaceDetectedEventArgs() { Data = faceEvent ,FaceList = faceEvents});
                            if (faceEvent.IsMatch)
                            {
                                MatchCount++;

                                Alert?.Invoke(sender, new FaceDetectedEventArgs() { Data = faceEvent });
                            }
                            else
                            {
                                AppearCount++;
                            }

                        }

                        NotifyDataChanged();
                    }
                }
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
            }
        }

        private void Cortica_Error(object sender, CorticaErrorEventArgs e)
        {
            Error?.Invoke(this, new ErrorEventArgs() { ErrorMsg = e.Message });
        }
    }
}
