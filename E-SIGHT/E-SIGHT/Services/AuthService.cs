﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace E_SIGHT.Services
{
    public class AuthService
    {
        public Root tokenObject = new Root();
        public static async Task<string> PostAuth()
        {
            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback = delegate { return true; };
            HttpClient client = new HttpClient(clientHandler);

            string username = "superadmin";
            string password = "qwer1234";

            var values = new Dictionary<string, string>();
            values.Add("username", username);
            values.Add("password", password);

            var response = await client.PostAsync("https://192.168.1.158:5004/auth/login/", new FormUrlEncodedContent(values));
            response.EnsureSuccessStatusCode();

            string content = await response.Content.ReadAsStringAsync();
            Root tokenObject = JsonConvert.DeserializeObject<Root>(content);
            Console.WriteLine(tokenObject.access_token);
            client.Dispose();
            return tokenObject.access_token;
        }
    }

    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class Data
    {
        public bool active { get; set; }
        public double create_time_utc { get; set; }
        public string name { get; set; }
        public List<object> permission_names { get; set; }
        public string role { get; set; }
        public string status { get; set; }
        public string token { get; set; }
        public string user_id { get; set; }
    }

    public class Metadata
    {
        public string msg { get; set; }
    }

    public class Root
    {
        public string access_token { get; set; }
        public Data data { get; set; }
        public double expires_in { get; set; }
        public Metadata metadata { get; set; }
        public string token_type { get; set; }
    }

}
