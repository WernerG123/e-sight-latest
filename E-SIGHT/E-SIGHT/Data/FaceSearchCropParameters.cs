﻿namespace E_SIGHT.Data
{
    public class FaceSearchCropParameters
    {
        public string CropImg { get; set; }
        public bool Selected { get; set; }
    }
}
