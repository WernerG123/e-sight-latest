﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E_SIGHT.Data
{
    public class ServerSettings
    {
        public string ServerName { get; set; }
        public string ServerAbbrv { get; set; }
        public string ListenerUrl { get; set; }
        public string CameraApiUrl { get; set; }
        public string PoiApiUrl { get; set; }
    }

}
