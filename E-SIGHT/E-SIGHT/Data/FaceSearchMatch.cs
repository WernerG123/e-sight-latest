﻿using CorticaIntegration.CorticaData.PoiDb.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E_SIGHT.Data
{
    public class FaceSearchMatch
    {
        public string ServerName { get; set; }
        public string ServerAbbrv { get; set; }
        public string PoiName { get; set; }
        public string PoiId { get; set; }
        public string Img { get; set; }
        public float Confidence { get; set; }
        public IEnumerable<WatchlistData> Watchlists { get; set; }
    }
}
