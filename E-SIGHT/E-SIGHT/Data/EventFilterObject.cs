﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E_SIGHT.Data
{
    public class EventFilterObject
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public string ServerName { get; set; }
        public string ServerAbbrv { get; set; }
        public string DisplayName { get { return $"{ServerAbbrv}.{Description}"; } }
        public bool CheckedVal { get; set; }
    }
}
