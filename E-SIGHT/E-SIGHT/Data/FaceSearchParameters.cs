﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E_SIGHT.Data
{
    public class FaceSearchParameters
    {
        public string baseImg { get; set; }
        public IList<FaceSearchCropParameters> Crops { get; private set; } = new List<FaceSearchCropParameters>();
        public string FaceImg { get; set; }
        public float MinConfidence { get; set; } = 50;
        public int MaxMatchesPerServer { get; set; } = 10;

        public void Select(FaceSearchCropParameters crop)
        {
            foreach(var c in Crops)
            {
                if(c == crop)
                {
                    c.Selected = true;
                }
                else
                {
                    c.Selected = false;
                }
            }
        }
    }
}
