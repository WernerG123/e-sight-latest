﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using CorticaIntegration.CorticaData.PoiDb.Shared;
using CorticaIntegration.CorticaData.Event;

namespace E_SIGHT.Data
{
    public class FaceDetectedEvent
    {
        public string ServerName { get; set; }

        public string ServerAbbrv { get; set; }

        public string EventId { get; set; }

        public DateTime FrameDateTime { get; set; }

        public DateTime AppearanceDateTime { get; set; }

        public string AppearanceId { get; set; }

        public bool IsMatch { get { return !string.IsNullOrWhiteSpace(this.PoiName) && !string.IsNullOrWhiteSpace(this.PoiImage); } }

        public int Tracked { get; set; } = 1;

        public string PoiName { get; set; }

        public string PoiImage { get; set; }

        public string CropImage { get; set; }

        public double? FaceScore { get; set; }

        public double? Confidence { get; set; }

        public string CameraId { get; set; }

        public string CameraName { get; set; }

        public IEnumerable<CorticaIntegration.CorticaData.Event.Watchlist> Watchlists { get; set; }

        public string WatchlistCsv { get { return string.Join(", ", Watchlists.Select((w) => w.Display_name)); } }

    }
}
