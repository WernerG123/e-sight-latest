﻿using E_SIGHT.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace E_SIGHT.EventArguments
{
    public class FaceDetectedEventArgs : EventArgs
    {
        public FaceDetectedEvent Data { get; set; }
        public IEnumerable<FaceDetectedEvent> FaceList { get; set;}

    }
}
