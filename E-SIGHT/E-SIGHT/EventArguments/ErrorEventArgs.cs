﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;

namespace E_SIGHT.EventArguments
{
    public class ErrorEventArgs : EventArgs
    {
        public string ErrorMsg { get; set; }
    }
}
