/* Navbar - Active */
$('.navbar-nav .nav-link').click(function () {
    $('.navbar-nav .nav-link').removeClass('active');
    $(this).addClass('active');
})

$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
});

function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}

/* Dropdown Lists */

/* Dropdown List - Cameras
var checkList = document.getElementById('cameras');
checkList.getElementsByClassName('anchor')[0].onclick = function (evt) {
    if (checkList.classList.contains('visible'))
        checkList.classList.remove('visible');
    else
        checkList.classList.add('visible');
}

checkList.onblur = function (evt) {
    checkList.classList.remove('visible');
}

/* Dropdown List - Watchlists
var checkList = document.getElementById('watchlists');
checkList.getElementsByClassName('anchor')[0].onclick = function (evt) {
    if (checkList.classList.contains('visible'))
        checkList.classList.remove('visible');
    else
        checkList.classList.add('visible');
}

checkList.onblur = function (evt) {
    checkList.classList.remove('visible');
}

/* Dropdown List - Time
var checkList = document.getElementById('time');
checkList.getElementsByClassName('anchor')[0].onclick = function (evt) {
    if (checkList.classList.contains('visible'))
        checkList.classList.remove('visible');
    else
        checkList.classList.add('visible');
}

checkList.onblur = function (evt) {
    checkList.classList.remove('visible');
}*/